const selectElement = document.getElementById("color-select");
selectElement.addEventListener("change", changeBackgroundColor);

document.body.style.backgroundColor = "green";

const savedColor = localStorage.getItem("selectedColor");
if (savedColor) {
  selectElement.value = savedColor;
  changeBackgroundColor();
}

function changeBackgroundColor() {
  const selectedColor = selectElement.value;

  switch (selectedColor) {
    case "green":
      document.body.style.backgroundColor = "green";
      break;
    case "black":
      document.body.style.backgroundColor = "black";
      break;
    default:
      document.body.style.backgroundColor = "green";
      break;
  }

  localStorage.setItem("selectedColor", selectedColor);
}
