const root = document.getElementById("root");
const ul = document.createElement("ul");
root.append(ul);

const validKeys = ["author", "name", "price"];

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    // name: "Коти в мистецтві",
  },
];

function createList(objectArr) {
  function isValidKey(object, validKeys, index) {
    let validKeysArr = [];
    validKeys.forEach(function (validKey) {
      if (!(validKey in object)) {
        validKeysArr.push(validKey);
      }
    });
    if (validKeysArr.length !== 0)
      throw new Error(
        `Властивості ${validKeysArr} немає у об'єкта під індексом ${index}`
      );
  }

  objectArr.map(function (object, index) {
    try {
      isValidKey(object, validKeys, index);
      const li = document.createElement("li");
      li.innerHTML = `Автор: ${object.author}, Назва книги: ${object.name}, Ціна: ${object.price}`;
      ul.append(li);
    } catch (e) {
      console.error(e.message);
    }
  });
}

createList(books);
