function createNewUser() {
  let _firstName, _lastName, _birthday;

  do {
    _firstName = prompt("Введіть ваше ім'я:");
  } while (!_firstName.trim());

  do {
    _lastName = prompt('Введіть ваше прізвище:');
  } while (!_lastName.trim());

  do {
    _birthday = prompt('Введіть вашу дату народження (формат: dd.mm.yyyy):');
  } while (
    !/^(0[1-9]|[1-2][0-9]|3[0-1])\.(0[1-9]|1[0-2])\.(19|20)\d{2}$/.test(
      _birthday,
    ) ||
    new Date().getFullYear() < _birthday.slice(6, 10)
  );

  let newUser = {
    _firstName,
    _lastName,
    _birthday,
    getFirstName: function () {
      return this._firstName;
    },
    setFirstName: function (newFirstName) {
      if (!newFirstName?.trim()) throw new Error("Iм'я не може бути пустим!");
      this._firstName = newFirstName;
    },
    getLastName: function () {
      return this._lastName;
    },
    setLastName: function (newLastName) {
      if (!newLastName?.trim())
        throw new Error('Прізвище не може бути пустим!');
      this._lastName = newLastName;
    },
    getLogin: function () {
      return (this._firstName[0] + this._lastName).toLowerCase();
    },
    getAge: function () {
      const today = new Date();
      const birthDate = new Date(this._birthday.split('.').reverse().join('-'));
      let age = today.getFullYear() - birthDate.getFullYear();
      const monthDiff = today.getMonth() - birthDate.getMonth();
      if (
        monthDiff < 0 ||
        (monthDiff === 0 && today.getDate() < birthDate.getDate())
      ) {
        age--;
      }
      return age;
    },
    getPassword: function () {
      return `${this._firstName[0].toUpperCase()}${this._lastName.toLowerCase()}${this._birthday.slice(
        6,
      )}`;
    },
  };
  return Object.seal(newUser);
}

let user = createNewUser();
// Одержані дані з prompt;
console.log(user.getLogin());

user.setFirstName('John');
user.setLastName('Dou');
// Одержані дані через сетери;
console.log(user.getLogin());

console.log('Age: ', user.getAge());
console.log('Password: ', user.getPassword());

user.firstName = 'Adam';
user.lastName = 'Li';
// Невдала спроба змінити дані напряму;
console.log(user.getLogin());
// Кастомні властивості firstName та lastName в об'єкт user не додались.
console.log(user);
