# Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування/

Це процес додавання слеша перед символом, що має спеціальне значення, щоб
відобразити його як звичайний символ а не елемент коду. Наприклад, символ лапок
" використовується для позначення рядкових значень. Якщо в рядку містяться самі
лапки, то їх потрібно екранувати, щоб комп'ютер зрозумів, що це звичайний
символ, а не початок або кінець рядкового значення.

Без екранування спеціальні символи будуть викликати помилки.

# Які засоби оголошення функцій ви знаєте?

Читала про 3 засоби: Оголошення функції (Function Declaration), Функціональний
вираз (Function Expression), Стрілкові функції (Arrow Functions). На практиці
використовувала лише перших два.

# Що таке hoisting, як він працює для змінних та функцій?

Hoisting дозволяє викликати функції та використовувати змінні до їх оголошення у
коді. Для змінних, означає, що їх можна використовувати до їх оголошення, але на
цей момент значення буде undefined. Наприклад, якщо ми спробуємо викликати
змінну оголошену через:

- let, const JavaScript видасть помилку
- var - не видасть помилку, а замість значення виведе undefined:

Аналогічно з функціями, Function Declaration може бути викликана вище свого
оголошення в коді, Function Expression - ні, буде помилка.
