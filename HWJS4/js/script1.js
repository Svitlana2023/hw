let num1;
let num2;
while (true) {
  num1 = prompt('Введіть перше число:', num1);
  num2 = prompt('Введіть друге число:', num2);
  if (isNaN(num1) || isNaN(num2)) {
    alert('Ви ввели неправильні значення. Спробуйте ще раз.');
  } else {
    break;
  }
}
let operator;
while (true) {
  operator = prompt('Введіть операцію (+, -, *, /):');
  if (
    operator === '+' ||
    operator === '-' ||
    operator === '*' ||
    operator === '/'
  ) {
    break;
  }
  alert('Введіть допустиму операцію: +, -, *, /');
}

const calculate = function () {
  num1 = parseInt(num1);
  num2 = parseInt(num2);
  switch (operator) {
    case '+':
      return num1 + num2;
    case '-':
      return num1 - num2;
    case '*':
      return num1 * num2;
    case '/':
      return num1 / num2;
    default:
      return NaN;
  }
};

// Варіант 2- це не мій, але дуже цікавий розв’язок
// const calculate = function () {
//   return eval(`${num1} ${operator} ${num2}`);
// };

let result = calculate();
console.log(result);
