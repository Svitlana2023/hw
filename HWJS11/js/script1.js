document.addEventListener("DOMContentLoaded", function () {
  const passwordInput = document.getElementById("password-input");
  const confirmPasswordInput = document.getElementById(
    "confirm-password-input"
  );
  const confirmButton = document.querySelector(".password-form .btn");
  const errorText = document.createElement("p");
  errorText.textContent = "Потрібно ввести однакові значення";
  errorText.style.color = "red";
  errorText.style.display = "none";
  confirmPasswordInput.parentNode.appendChild(errorText);

  function togglePasswordVisibility(input, icon) {
    icon.classList.toggle("fa-eye-slash");
    if (input.type === "password") {
      input.type = "text";
    } else {
      input.type = "password";
    }
  }

  function handleFormClick(event) {
    const target = event.target;

    if (target.classList.contains("icon-password")) {
      const input = target.previousElementSibling;
      togglePasswordVisibility(input, target);
    }
  }

  function handleFormSubmit(event) {
    event.preventDefault();

    if (passwordInput.value === confirmPasswordInput.value) {
      alert("You are welcome");
      errorText.style.display = "none";
    } else {
      errorText.style.display = "block";
    }
  }

  document
    .querySelector(".password-form")
    .addEventListener("click", handleFormClick);
  confirmButton.addEventListener("click", handleFormSubmit);
});
