function createList(array, parent = document.body) {
  const list = document.createElement('ul');

  array.map(function (item) {
    const listItem = document.createElement('li');
    if (Array.isArray(item)) {
      createList(item, listItem); // Рекурсивний виклик для вкладеного масиву
    } else {
      listItem.textContent = item;
    }
    list.appendChild(listItem);
  });

  parent.appendChild(list);
}

const myArray = [
  'hello',
  'world',
  'Kiev',
  'Kharkiv',
  ['Borispol', 'Irpin'],
  'Odessa',
  'Lviv',
  'Dnieper',
];
createList(myArray);

const timer = document.createElement('div');
timer.textContent = '3';
timer.style.color = 'red';
timer.style.fontSize = '60px';

document.body.appendChild(timer);

let count = 3;
const intervalId = setInterval(() => {
  count--;
  timer.textContent = count;
  if (count === 0) {
    clearInterval(intervalId);
    document.body.innerHTML = '';
  }
}, 1000);
