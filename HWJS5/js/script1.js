
// Варіант 1
// function createNewUser() {
//   let firstName, lastName;

//   do {
//     firstName = prompt("Введіть ваше ім'я:");
//   } while (!firstName.trim());

//   do {
//     lastName = prompt('Введіть ваше прізвище:');
//   } while (!lastName.trim());

//   const newUser = {};

//   Object.defineProperties(newUser, {
//     firstName: {
//       get: function () {
//         return firstName;
//       },
//       configurable: false,
//     },
//     lastName: {
//       get: function () {
//         return lastName;
//       },
//       configurable: false,
//     },
//     getLogin: {
//       value: function () {
//         return (this.firstName[0] + this.lastName).toLowerCase();
//       },
//     },
//     setFirstName: {
//       value: function (newFirstName) {
//         if (!newFirstName?.trim()) throw new Error("Iм'я не може бути пустим!");
//         firstName = newFirstName;
//       },
//     },
//     setLastName: {
//       value: function (newLastName) {
//         if (!newLastName?.trim())
//           throw new Error('Прізвище не може бути пустим!');
//         lastName = newLastName;
//       },
//     },
//   });
//   return newUser;
// }

// Варіант 2

function createNewUser() {
  let _firstName, _lastName;

  do {
    _firstName = prompt("Введіть ваше ім'я:");
  } while (!_firstName.trim());

  do {
    _lastName = prompt('Введіть ваше прізвище:');
  } while (!_lastName.trim());

  let newUser = {
    _firstName,
    _lastName,
    getFirstName: function () {
      return this._firstName;
    },
    setFirstName: function (newFirstName) {
      if (!newFirstName?.trim()) throw new Error("Iм'я не може бути пустим!");
      this._firstName = newFirstName;
    },
    getLastName: function () {
      return this._lastName;
    },
    setLastName: function (newLastName) {
      if (!newLastName?.trim())
        throw new Error('Прізвище не може бути пустим!');
      this._lastName = newLastName;
    },
    getLogin: function () {
      return (this._firstName[0] + this._lastName).toLowerCase();
    },
  };
  return Object.seal(newUser);
}

let user = createNewUser();
// Одержані дані з prоmpt;
console.log(user.getLogin());

user.setFirstName('John');
user.setLastName('Dou');
// Одержані дані через сетери;
console.log(user.getLogin());

user.firstName = 'Adam';
user.lastName = 'Li';
// Невдла спроба змінити дані напряму;
console.log(user.getLogin());
// Кастомні властивості firstName та lastName в обєкт user не додались.
console.log(user);
