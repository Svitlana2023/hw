#1. Опишіть своїми словами, що таке метод об'єкту
Метод - це функція в об’єкті. В методі обєкту можна використати ключове слово this, тобто це звернення до обєкту в якому знаходиться метод.

#2. Який тип даних може мати значення властивості об'єкта?
Значення властивості об'єкта може бути будь-якого типу даних - числа, рядки, об'єкти, масиви, функції та ін., що робить об'єкти потужним засобом для зберігання та обробки різноманітних даних.

#3. Об'єкт це посилальний тип даних. Що означає це поняття?
Це означає, що змінна, що містить об'єкт, насправді містить не сам об'єкт, а посилання на об'єкт, яке вказує на його місцезнаходження в пам'яті.
