document.addEventListener("DOMContentLoaded", function () {
  const tabsContainer = document.querySelector(".service-tabs");
  tabsContainer.addEventListener("click", function (event) {
    if (event.target.matches("[data-tab]")) {
      const data = event.target.getAttribute("data-tab");
      const activeTab = document.querySelector(".active-tabs");
      const activeContent = document.querySelector(".active-tab");

      activeTab.classList.remove("active-tabs");
      activeContent.classList.remove("active-tab");

      event.target.classList.add("active-tabs");
      document.querySelector(data).classList.add("active-tab");
    }
  });

  const photoTables = document.querySelector(".photo-tables");
  let activeButton = null;

  function switchButton(button) {
    if (activeButton) {
      activeButton.classList.remove("active");
    }

    button.classList.add("active");
    activeButton = button;
  }

  function switchTab(category) {
    const buttons = photoTables.querySelectorAll(".photo-button");

    buttons.forEach((button) => {
      button.classList.remove("active-tabs");
      if (button.getAttribute("data-filter") === category) {
        button.classList.add("active-tabs");
      }
    });
  }

  photoTables.addEventListener("click", function (event) {
    const button = event.target.closest(".photo-button");
    if (button) {
      switchButton(button);
      const category = button.getAttribute("data-filter");
      switchTab(category);
    }
  });

  photoTables.addEventListener("mouseenter", function (event) {
    const button = event.target.closest(".photo-button");
    if (button && !button.classList.contains("active")) {
      button.classList.add("hover");
    }
  });

  photoTables.addEventListener("mouseleave", function (event) {
    const button = event.target.closest(".photo-button");
    if (button && !button.classList.contains("active")) {
      button.classList.remove("hover");
    }
  });
});

const photoTables = document.querySelector(".photo-tables");
const loadMoreButton = document.querySelector(".button-explore-more");
let isLoadMoreClicked = false;

photoTables.addEventListener("click", function (event) {
  const target = event.target;

  if (target.classList.contains("photo-button")) {
    const filter = target.getAttribute("data-filter");
    const photoItems = document.querySelectorAll(".photo-item");

    photoItems.forEach(function (item, index) {
      const category = item.getAttribute("data-category");

      if (!isLoadMoreClicked) {
        if (filter === "all") {
          if (index < 12) {
            item.style.display = "inline-block";
          } else {
            item.style.display = "none";
          }
        } else if (filter === category) {
          item.style.display = "inline-block";
        } else {
          item.style.display = "none";
        }
      } else {
        if (filter === "all" || filter === category) {
          item.style.display = "inline-block";
        } else {
          item.style.display = "none";
        }
      }
    });

    if (filter === "all" && !isLoadMoreClicked) {
      loadMoreButton.style.display = "inline-block";
    } else {
      loadMoreButton.style.display = "none";
    }
  }
});

function handleLoadMore() {
  const hiddenItems = document.querySelectorAll(".photo-item.hidden");

  hiddenItems.forEach(function (item, index) {
    item.style.display = "inline-block";
    item.classList.remove("hidden");
  });

  loadMoreButton.style.display = "none";

  isLoadMoreClicked = true;

  const activeFilter = document.querySelector(".photo-button.active");
  if (activeFilter) {
    activeFilter.click();
  }
}

loadMoreButton.addEventListener("click", handleLoadMore);

// Слайдер

document.addEventListener("DOMContentLoaded", function () {
  const filterName = document.querySelectorAll("[data-name]");
  const filterBtn = document.querySelectorAll("[data-btn]");
  const images = document.querySelectorAll(".slider-item");
  const firstImage = images[0];
  const lastImage = images[images.length - 1];

  function removeClass(classToRemove, direction) {
    const currentItem = document.querySelector(`.${classToRemove}`);

    document
      .querySelector(`.${classToRemove}`)
      .classList.remove(`${classToRemove}`);

    if (direction === "prev") {
      currentItem.previousElementSibling.classList.add(`${classToRemove}`);
    }
    if (direction === "next") {
      currentItem.nextElementSibling.classList.add(`${classToRemove}`);
    }
  }

  filterName.forEach(function (name) {
    name.addEventListener("click", function () {
      const cat = this.getAttribute("data-name");
      removeClass("visible-slider");
      removeClass("active-slider");
      this.classList.add("active-slider");
      document.querySelector(cat).classList.add("visible-slider");
    });
  });

  filterBtn.forEach(function (btn) {
    btn.addEventListener("click", function () {
      const cat = this.getAttribute("data-btn");
      const currentImage = document.querySelector(".active-slider");
      const currentItem = document.querySelector(".visible-slider");

      if (currentImage !== firstImage && cat === "prev") {
        removeClass("active-slider", "prev");
        removeClass("visible-slider", "prev");
      }

      if (currentImage !== lastImage && cat === "next") {
        removeClass("active-slider", "next");
        removeClass("visible-slider", "next");
      }
    });
  });
});
