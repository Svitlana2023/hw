import React from "react";
import "./Modal.css";

const Modal = ({ header, closeButton, text, actions, closeModal }) => {
  return (
    <div className="modal-overlay" onClick={closeModal}>
      <div className="modal">
        <div className="modal-header">
          {header}
          {closeButton && (
            <button className="modal-close-button" onClick={closeModal}>
              ⨉
            </button>
          )}
        </div>
        <div className="modal-content">{text}</div>
        <div className="modal-actions">{actions}</div>
      </div>
    </div>
  );
};

export default Modal;
