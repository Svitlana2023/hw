import React from "react";

const Button = ({ backgroundColor, text, onClick }) => {
  return (
    <button
      style={{ backgroundColor }}
      className="custom-button"
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default Button;
