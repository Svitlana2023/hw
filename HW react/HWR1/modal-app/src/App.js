import React, { useState } from "react";
import "./App.css";
import Button from "./components/Button";
import Modal from "./components/Modal";

function App() {
  const [firstModalOpen, setFirstModalOpen] = useState(false);
  const [secondModalOpen, setSecondModalOpen] = useState(false);

  const openFirstModal = () => {
    setFirstModalOpen(true);
  };

  const closeFirstModal = () => {
    setFirstModalOpen(false);
  };

  const openSecondModal = () => {
    setSecondModalOpen(true);
  };

  const closeSecondModal = () => {
    setSecondModalOpen(false);
  };

  return (
    <div className="App">
      <header className="App-header">
        <Button
          backgroundColor="#3498db"
          text="Open first modal"
          onClick={openFirstModal}
        />
        <Button
          backgroundColor="#e74c3c"
          text="Open second modal"
          onClick={openSecondModal}
        />
      </header>
      {firstModalOpen && (
        <Modal
          header="Do you want to delete this file?"
          closeButton={true}
          text="Onse you delete this file, it won’t be undo this action. Are you sure you want to delete it?"
          actions={
            <>
              <button onClick={closeFirstModal}>Ok</button>{" "}
              <button onClick={closeFirstModal}>Cancel</button>
              {/* Закриття модального вікна */}
            </>
          }
          closeModal={closeFirstModal}
        />
      )}
      {secondModalOpen && (
        <Modal
          header="Are you sure you want to delete this file?"
          closeButton={true}
          text="Are you certain you want to delete this file? There's no going back."
          actions={
            <>
              <button onClick={closeSecondModal}>Так</button>{" "}
              <button onClick={closeSecondModal}>Вихід</button>
              {/* Закриття модального вікна */}
            </>
          }
          closeModal={closeSecondModal}
        />
      )}
    </div>
  );
}

export default App;
