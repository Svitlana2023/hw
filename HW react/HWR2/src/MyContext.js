import React, { createContext, useContext, useState } from "react";

const MyContext = React.createContext();

export const MyContextProvider = ({ children }) => {
  const [cart, setCart] = useState([]); // Початковий стан кошика
  const [favorites, setFavorites] = useState([]); // Початковий стан обраного

  // Функція для додавання товару у кошик
  const addToCart = (product) => {
    setCart([...cart, product]);
  };

  // Функція для додавання товару до обраного
  const addToFavorites = (product) => {
    setFavorites([...favorites, product]);
  };

  return (
    <MyContext.Provider value={{ cart, favorites, addToCart, addToFavorites }}>
      {children}
    </MyContext.Provider>
  );
};

export const useMyContext = () => {
  return useContext(MyContext);
};
