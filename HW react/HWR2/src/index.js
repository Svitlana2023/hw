import React from "react";
import ReactDOM from "react-dom/client";
// import HomePage from "./components/HomePage"; // Імпортуємо компонент HomePage
import App from "./App"; // Імпортуємо компонент HomePage
import "./index.css";

// import { createRoot } from "react-dom/client";
import { MyContextProvider } from "./MyContext"; // Імпортуємо ваш ContextProvider

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <MyContextProvider>
    <App />
  </MyContextProvider>
);
