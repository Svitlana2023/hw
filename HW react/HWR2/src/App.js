import React from "react";
import { MyContextProvider } from "./MyContext";
// import Header from "../src/components/Header";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import HomePage from "../src/components/HomePage";
import CartPage from "../src/components/CartPage";
import FavoritesPage from "../src/components/FavoritesPage";

function App() {
  return (
    <MyContextProvider>
      <Router>
        <div className="App">
          {/* <Header /> */}
          <Routes>
            <Route path="/cart" element={<CartPage />} />
            <Route path="/favorites" element={<FavoritesPage />} />
            <Route path="/" element={<HomePage />} />
          </Routes>
        </div>
      </Router>
    </MyContextProvider>
  );
}

export default App;
