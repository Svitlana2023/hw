import React, { useState } from "react";
import Modal from "react-modal";

Modal.setAppElement("#root");

const CartAndFavoritesModal = ({
  cartItems,
  favoritesItems,
  onClearCart,
  onClearFavorites,
  onRequestClose,
}) => {
  const [activeTab, setActiveTab] = useState("cart");

  const switchToCart = () => setActiveTab("cart");
  const switchToFavorites = () => setActiveTab("favorites");

  return (
    <Modal
      isOpen={true}
      onRequestClose={onRequestClose}
      contentLabel="Cart and Favorites Modal"
    >
      <h2>Кошик та Обрані</h2>
      <div className="modal-tabs">
        <button
          className={activeTab === "cart" ? "active" : ""}
          onClick={switchToCart}
        >
          Кошик
        </button>
        <button
          className={activeTab === "favorites" ? "active" : ""}
          onClick={switchToFavorites}
        >
          Обрані
        </button>
      </div>

      {activeTab === "cart" ? (
        <div>
          <h3>Товари у кошику:</h3>
          <ul>
            {cartItems.map((item) => (
              <li key={item.id}>{item.name}</li>
            ))}
          </ul>
          <button onClick={onClearCart}>Очистити кошик</button>
        </div>
      ) : (
        <div>
          <h3>Обрані товари:</h3>
          <ul>
            {favoritesItems.map((item) => (
              <li key={item.id}>{item.name}</li>
            ))}
          </ul>
          <button onClick={onClearFavorites}>Очистити обране</button>
        </div>
      )}
    </Modal>
  );
};

export default CartAndFavoritesModal;
