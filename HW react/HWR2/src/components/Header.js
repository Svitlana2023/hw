import React from "react";
import { Link } from "react-router-dom";
import { FaShoppingCart, FaHeart } from "react-icons/fa";
import { useMyContext } from "../MyContext";

const Header = () => {
  const { cart, favorites } = useMyContext();

  return (
    <div className="header">
      <Link to="/cart" className="cart">
        <FaShoppingCart />
        <span>Кошик: {cart.length} товарів</span>
      </Link>
      <Link to="/favorites" className="favorites">
        <FaHeart />
        <span>Обране: {favorites.length} товарів</span>
      </Link>
    </div>
  );
};

export default Header;
// import React, { useState } from "react";
// import CartAndFavoritesModal from "./CartAndFavoritesModal";

// const Header = ({ cartCount, favoritesCount }) => {
//   const [isModalOpen, setIsModalOpen] = useState(false);

//   const openModal = () => {
//     setIsModalOpen(true);
//   };

//   const closeModal = () => {
//     setIsModalOpen(false);
//   };

//   return (
//     <div className="header">
//       <h1>Магазин</h1>
//       <div className="header-buttons">
//         <button onClick={openModal}>Кошик ({cartCount})</button>
//         <button onClick={openModal}>Обрані ({favoritesCount})</button>
//       </div>
//       {isModalOpen && (
//         <CartAndFavoritesModal
//           cartItems={/* Передайте товари у кошику */}
//           favoritesItems={/* Передайте обрані товари */}
//           onClearCart={/* Додайте функцію для очищення кошика */}
//           onClearFavorites={/* Додайте функцію для очищення обраних */}
//           onRequestClose={closeModal}
//         />
//       )}
//     </div>
//   );
// };

// export default Header;
