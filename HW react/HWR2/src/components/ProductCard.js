// import React, { Component } from "react";

// class ProductCard extends Component {
//   render() {
//     const { product, onAddToCart } = this.props;

//     return (
//       <div className="product-card">
//         <img src={product.image} alt={product.name} />
//         {/* <img src="img/goods/pear.png" alt={product.name} /> */}
//         <h3>{product.name}</h3>
//         <p>Ціна: ${product.price}</p>
//         <p>Колір: {product.color}</p>
//         <button onClick={onAddToCart}>Додати в кошик</button>
//       </div>
//     );
//   }
// }

// export default ProductCard;

// import React from "react";

// const ProductCard = ({ product, onAddToCart }) => {
//   return (
//     <div className="product-card">
//       {/* Інформація про товар */}
//       <img src={product.image} alt={product.name} />
//       {/* <img src="img/goods/pear.png" alt={product.name} /> */}
//       <h3>{product.name}</h3>
//       <p>{product.description}</p>
//       <p>Price: ${product.price}</p>

//       {/* Кнопка "Add to Cart" */}
//       <button onClick={onAddToCart}>Add to Cart</button>
//     </div>
//   );
// };

// export default ProductCard;

import React, { useState, useEffect } from "react";
import { useMyContext } from "../MyContext";

const ProductCard = ({ product, onAddToCart }) => {
  const [isFavorite, setIsFavorite] = useState(
    JSON.parse(localStorage.getItem("favoritesArr"))?.includes(product.id) ||
      false
  );
  const { favorites, addToFavorites } = useMyContext();
  // console.log("favorites", favorites);

  const toggleFavorite = () => {
    // Оновлюємо стан isFavorite та викликаємо функцію з батьківського компонента
    setIsFavorite(!isFavorite);

    // Оновлюємо дані в localStorage
    const favoritesArr = JSON.parse(localStorage.getItem("favoritesArr")) || [];
    if (isFavorite) {
      const updatedFavorites = favoritesArr.filter((id) => id !== product.id);
      localStorage.setItem("favoritesArr", JSON.stringify(updatedFavorites));
    } else {
      favoritesArr.push(product.id);
      localStorage.setItem("favoritesArr", JSON.stringify(favoritesArr));
      addToFavorites(product);
    }
    // console.log("favoritesArr", favoritesArr);
  };

  return (
    <div className="product-card">
      {/* Інформація про товар */}
      <img src={product.image} alt={product.name} width="100" height="75" />
      <h3>{product.name}</h3>
      <p>{product.description}</p>
      <p>Price: ${product.price}</p>

      {/* Кнопки "Add to Cart" та "Add to Favorites" */}
      <button onClick={onAddToCart}>Add to Cart</button>
      <button onClick={toggleFavorite}>
        {isFavorite ? (
          <span role="img" aria-label="Favorite" style={{ color: "gold" }}>
            ★
          </span>
        ) : (
          <span role="img" aria-label="Not Favorite">
            ☆
          </span>
        )}
      </button>
    </div>
  );
};

export default ProductCard;
