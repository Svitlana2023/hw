const AddToCartModal = ({ product, onClose, addToCart }) => {
  const handleAddToCart = () => {
    addToCart(product); // Викликаємо функцію addToCart при кліку на кнопку
    onClose(); // Закриваємо модальне вікно
  };

  return (
    <div className="modal">
      <div className="modal-content">
        <span onClick={onClose} className="close-button">
          &times;
        </span>
        <h2>Додати товар до кошика</h2>
        {product && (
          <div>
            <p>Ви впевнені, що хочете додати "{product.name}" до кошика?</p>
            <button onClick={onClose}>Скасувати</button>
            <button onClick={handleAddToCart}>Додати до кошика</button>
          </div>
        )}
      </div>
    </div>
  );
};

export default AddToCartModal;
