import React from "react";
import { useMyContext } from "../MyContext"; // Імпортуємо ваш власний хук

const CartPage = () => {
  // Використовуємо власний хук для отримання даних з контексту
  const { cart } = useMyContext();

  return (
    <div>
      <h2>Сторінка Кошика</h2>
      <p>Кількість товарів у кошику: {cart.length}</p>
      {/* Тут ви можете відображати вміст кошика або робити інші операції з даними */}
    </div>
  );
};

export default CartPage;
