// import React, { useState, useEffect } from "react";
// import axios from "axios";
// import ProductCard from "../components/ProductCard";
// import Header from "./Header";
// import AddToCartModal from "../components/AddToCartModal";
// import { useMyContext } from "../MyContext";
// import "../styles/styles.css";
// import "../styles/productCard.css";
// import "../styles/header.css";

// function HomePage() {
//   const [products, setProducts] = useState([]);
//   const [cart, setCart] = useState([]);
//   const [favorites, setFavorites] = useState([]);
//   const [showModal, setShowModal] = useState(false);
//   const [selectedProduct, setSelectedProduct] = useState(null);

//   useEffect(() => {
//     loadProducts();

//     const cartItems = JSON.parse(localStorage.getItem("cart")) || [];
//     const favorites = JSON.parse(localStorage.getItem("favorites")) || [];

//     setCart(cartItems);
//     setFavorites(favorites);
//   }, []);

//   const loadProducts = () => {
//     axios
//       .get("/products.json") // Ваш URL для JSON файлу
//       .then((response) => {
//         setProducts(response.data);
//       })
//       .catch((error) => {
//         console.error("Помилка завантаження даних", error);
//       });
//   };

//   const addToCart = (product) => {
//     const cartItems = [...cart, product];
//     setCart(cartItems);
//     localStorage.setItem("cart", JSON.stringify(cartItems));
//   };

//   const addToFavorites = (product) => {
//     const newFavorites = [...favorites, product];
//     setFavorites(newFavorites);
//     localStorage.setItem("favorites", JSON.stringify(newFavorites));
//   };

//   const openModal = (product) => {
//     setShowModal(true);
//     setSelectedProduct(product);
//   };

//   const closeModal = () => {
//     setShowModal(false);
//     setSelectedProduct(null);
//   };

//   return (
//     <div>
//       <Header cartCount={cart.length} favoritesCount={favorites.length} />

//       <div className="product-list">
//         {products.map((product) => (
//           <div key={product.id}>
//             <ProductCard
//               product={product}
//               onAddToCart={() => openModal(product)}
//               onAddToFavorites={() => addToFavorites(product)}
//             />
//           </div>
//         ))}
//       </div>

//       {showModal && (
//         <AddToCartModal
//           product={selectedProduct}
//           onClose={closeModal}
//           addToCart={addToCart}
//         />
//       )}
//     </div>
//   );
// }

// export default HomePage;

import React, { useState, useEffect } from "react";
import axios from "axios";
import ProductCard from "../components/ProductCard";
import Header from "../components/Header";
import AddToCartModal from "../components/AddToCartModal";
import { useMyContext } from "../MyContext";
import "../styles/styles.css";
import "../styles/productCard.css";
import "../styles/header.css";

function HomePage() {
  const { cart, addToCart } = useMyContext();
  const [products, setProducts] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);

  useEffect(() => {
    loadProducts();
  }, []);

  const loadProducts = () => {
    axios
      .get("/products.json") // Ваш URL для JSON файлу
      .then((response) => {
        setProducts(response.data);
      })
      .catch((error) => {
        console.error("Помилка завантаження даних", error);
      });
  };

  const closeModal = () => {
    setShowModal(false);
    setSelectedProduct(null);
  };

  const handleAddToCart = (product) => {
    // addToCart(product);
    setShowModal(true);
    setSelectedProduct(product);
  };

  return (
    <div>
      <Header cartCount={cart.length} />

      <div className="product-list">
        {products.map((product) => (
          <div key={product.id}>
            <ProductCard
              product={product}
              onAddToCart={() => handleAddToCart(product)}
            />
          </div>
        ))}
      </div>

      {showModal && (
        <AddToCartModal
          product={selectedProduct}
          onClose={closeModal}
          addToCart={addToCart}
        />
      )}
    </div>
  );
}

export default HomePage;
