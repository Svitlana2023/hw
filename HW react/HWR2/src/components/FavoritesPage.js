import React from "react";
import { useMyContext } from "../MyContext"; // Імпортуємо ваш власний хук

const FavoritesPage = () => {
  // Використовуємо власний хук для отримання даних з контексту
  const { favorites } = useMyContext();

  return (
    <div>
      <h2>Сторінка Обраного</h2>
      <p>Кількість товарів у обраному: {favorites.length}</p>
      {/* Тут ви можете відображати вміст обраного або робити інші операції з даними */}
    </div>
  );
};

export default FavoritesPage;
