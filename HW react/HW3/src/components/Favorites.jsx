import React from "react";
import styles from "./styles/ProductsInCart.module.scss";

function Favorites(props) {
  return (
    <div>
      <h2>Favorites:</h2>
      <ul className={styles.card}>
        {props.selectedProducts.map((product) => (
          <li className={styles.productCard} key={product.id}>
            <h3> {product.brandName}</h3>
            <p>{product.price} $</p>
            <img className={styles.selectedImg} src={product.image} alt="" />
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Favorites;
