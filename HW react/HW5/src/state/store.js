import defaultReducer from "./reducer";
import thunk from "redux-thunk";
import { createStore, applyMiddleware } from "redux";

const initialState = {
  products: {
    productList: [], // Початковий стан для властивості productList
  },
  modal: {
    isModalOpen: false, // Початковий стан для модального вікна
  },
};

const store = createStore(defaultReducer, initialState, applyMiddleware(thunk));

export default store;
