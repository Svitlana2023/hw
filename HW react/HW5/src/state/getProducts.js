// actions.js
import { showProducts } from "./actions";

// Ця функція викликається за допомогою Redux Thunk
export const fetchProducts = () => {
  return async (dispatch) => {
    try {
      const response = await fetch("/Products.json");
      const data = await response.json();
      dispatch(showProducts(data));
    } catch (error) {
      console.error("Помилка завантаження даних з сервера:", error);
    }
  };
};
