import { SHOW_PRODUCTS } from "./actions";

function defaultReducer(state, action) {
  switch (action.type) {
    case SHOW_PRODUCTS:
      return { ...state, productList: [...action.payload] };
    default:
      return state;
  }
}

export default defaultReducer;
