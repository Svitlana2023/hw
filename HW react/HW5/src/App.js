import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Route, Routes } from "react-router-dom";
import Favorites from "./components/Favorites";
import FullScreenWrapper from "./components/FullScreenWrapper";
import Header from "./components/Header";
import Product from "./components/Product";
import ProductsInCart from "./components/ProductsInCart";
import "./main.scss";
import { fetchProducts } from "./state/getProducts";
import { selectProductList } from "./state/selectors";

const style = {
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
};

function App() {
  const [favoriteCount, setFavoriteCount] = useState(0);
  const [cartCount, setCartCount] = useState(0);
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [selectedProductsInCart, setSelectedProductsInCart] = useState([]);
  const [cartClear, setCartClear] = useState(true);
  const productList = useSelector(selectProductList);
  const dispatch = useDispatch();

  const addToCartProducts = (product) => {
    setSelectedProductsInCart([...selectedProductsInCart, product]);
  };

  const addToSelectedProducts = (product) => {
    setSelectedProducts([...selectedProducts, product]);
  };

  const removeFromCartProducts = (productId) => {
    const updatedProductsInCart = selectedProductsInCart.filter(
      (p) => p.id !== productId
    );
    const deleteCard = window.confirm("Delete this card?");
    if (deleteCard === true) {
      setSelectedProductsInCart(updatedProductsInCart);
      updateCartCount(-1);
    }
    localStorage.setItem(`inCartStatus_${productId}`, JSON.stringify(false));
  };

  const removeFromSelectedProducts = (product) => {
    const updatedSelectedProducts = selectedProducts.filter(
      (p) => p.id !== product.id
    );
    setSelectedProducts(updatedSelectedProducts);
  };

  useEffect(() => {
    const storedFavoriteCount = localStorage.getItem("favoriteCount");
    const storedInCartCount = localStorage.getItem("cartCount");

    if (storedFavoriteCount !== null) {
      setFavoriteCount(parseInt(storedFavoriteCount));
    }

    if (storedInCartCount !== null) {
      setCartCount(parseInt(storedInCartCount));
    }

    const storedSelectedProducts = localStorage.getItem("selectedProducts");
    if (storedSelectedProducts) {
      setSelectedProducts(JSON.parse(storedSelectedProducts));
    }
    const storedSelectedProductsInCart = localStorage.getItem(
      "selectedProductsInCart"
    );
    if (storedSelectedProductsInCart) {
      setSelectedProductsInCart(JSON.parse(storedSelectedProductsInCart));
    }
  }, []);

  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  useEffect(() => {
    const storedSelectedProductsInCart = localStorage.getItem(
      "selectedProductsInCart"
    );
    if (storedSelectedProductsInCart) {
      setSelectedProductsInCart(JSON.parse(storedSelectedProductsInCart));
    }

    const storedSelectedFavorites = localStorage.getItem(
      "selectedProductsInCart"
    );
    if (storedSelectedFavorites) {
      setSelectedProductsInCart(JSON.parse(storedSelectedFavorites));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem(
      "selectedProductsInCart",
      JSON.stringify(selectedProductsInCart)
    );
    selectedProductsInCart.length === 0
      ? setCartClear(true)
      : setCartClear(false);
  }, [selectedProductsInCart]);

  useEffect(() => {
    localStorage.setItem("selectedProducts", JSON.stringify(selectedProducts));
  }, [selectedProducts]);

  const updateFavoriteCount = (countChange) => {
    const newFavoriteCount = favoriteCount + countChange;

    if (newFavoriteCount >= 0) {
      setFavoriteCount(newFavoriteCount);
      localStorage.setItem("favoriteCount", newFavoriteCount.toString());
    } else if (newFavoriteCount <= 0) {
      setFavoriteCount(0);
    }
  };

  const updateCartCount = (countChange) => {
    const newCartCount = cartCount + countChange;

    if (newCartCount >= 0) {
      setCartCount(newCartCount);
      localStorage.setItem("cartCount", newCartCount.toString());
    } else if (newCartCount <= 0) {
      setCartCount(0);
    }
  };

  const onClearCart = () => {
    selectedProductsInCart.forEach((product) =>
      localStorage.setItem(`inCartStatus_${product.id}`, JSON.stringify(false))
    );
    setCartCount(0);
    localStorage.setItem("cartCount", JSON.stringify(0));
    setSelectedProductsInCart([]);
  };

  return (
    <>
      <Header favoriteCount={favoriteCount} cartCount={cartCount} />
      <Routes>
        <Route
          path="/"
          element={
            <FullScreenWrapper>
              <div style={style}>
                {productList ? (
                  productList.map((product) => (
                    <Product
                      key={product.id}
                      info={product}
                      updateFavoriteCount={updateFavoriteCount}
                      updateCartCount={updateCartCount}
                      deleteHandler={removeFromCartProducts}
                      addToSelectedProducts={addToSelectedProducts}
                      addToCartProducts={addToCartProducts}
                      removeFromSelectedProducts={removeFromSelectedProducts}
                    />
                  ))
                ) : (
                  <p>Loading...</p>
                )}
              </div>
            </FullScreenWrapper>
          }
        />
        <Route
          path="/cart"
          element={
            <ProductsInCart
              cartClear={cartClear}
              onClearCart={onClearCart}
              selectedProducts={selectedProductsInCart}
              removeFromCartProducts={removeFromCartProducts}
            />
          }
        />
        <Route
          path="/favorites"
          element={<Favorites selectedProducts={selectedProducts} />}
        />
        <Route path="*" element={<div>Not found Route</div>} />
      </Routes>
    </>
  );
}

export default App;
