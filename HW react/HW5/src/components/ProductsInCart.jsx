import React from "react";
import Form from "./Form";
import styles from "./styles/ProductsInCart.module.scss";

function ProductsInCart({
  cartClear,
  selectedProducts,
  removeFromCartProducts,
  onClearCart,
}) {
  const renderProducts = () => {
    if (cartClear) {
      return (
        <p
          style={{
            color: "red",
            fontSize: "24px",
            fontWeight: "700",
            textAlign: "center",
            marginTop: "40px",
          }}
        >
          Your cart is empty.
        </p>
      );
    } else {
      return (
        <ul className={styles.card}>
          {selectedProducts.map((product) => (
            <li className={styles.productCard} key={product.id}>
              <h3> {product.brandName}</h3>
              <p>{product.price} $</p>
              <img className={styles.selectedImg} src={product.image} alt="" />
              <button onClick={() => removeFromCartProducts(product.id)}>
                Delete Card
              </button>
            </li>
          ))}
        </ul>
      );
    }
  };

  return (
    <div>
      {!cartClear ? (
        <>
          <Form onClearCart={onClearCart} />
          <h2>Products in Cart:</h2>
        </>
      ) : null}
      {renderProducts()}
    </div>
  );
}

export default ProductsInCart;
