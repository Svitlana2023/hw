import PropTypes from "prop-types";
import React from "react";
import { Link } from "react-router-dom";
import Cart from "./images/cart.png";
import Favorite from "./images/favorite.png";
import styles from "./styles/Header.module.scss";

function Header({ favoriteCount, cartCount }) {
  return (
    <header>
      <nav>
        <ul className={styles.nav}>
          <Link className={styles.link} to="/">
            Home
          </Link>
          <Link className={styles.link} to="/favorites">
            <img src={Favorite} alt="Favorite" /> ({favoriteCount})
          </Link>

          <Link className={styles.link} to="/cart">
            <img src={Cart} alt="Cart" /> ({cartCount})
          </Link>
        </ul>
      </nav>
    </header>
  );
}

Header.propTypes = {
  favoriteCount: PropTypes.number.isRequired,
  cartCount: PropTypes.number.isRequired,
};

export default Header;
