import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useState } from "react";
import { PatternFormat } from "react-number-format";
import * as Yup from "yup";
import styles from "./styles/Form.module.scss";
const phoneRegExp =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .required("Name is required")
    .matches(
      /^[A-Za-zА-Яа-яІіЇїЄєҐґ]+$/,
      "Ім’я повидно містити лише ім’я, без цифер"
    ),
  lastname: Yup.string()
    .required("Lastname is required")
    .matches(
      /^[A-Za-zА-Яа-яІіЇїЄєҐґ]+$/,
      "Прізвище повидно містити лише прізвище, без цифер"
    ),
  age: Yup.number()
    .required("Age is required")
    .positive("Age must be a positive number")
    .integer("Age must be an integer")
    .min(16, "Age must be at least 16")
    .max(100, "Age must be less than or equal to 100"),
  address: Yup.string().required("Address is required"),
  phone: Yup.string()
    .matches(phoneRegExp, "Please write only numbers")
    .min(8, "too short")
    .max(10, "too long")
    .required("Please write your phone number"),
});

const CheckoutForm = ({ onClearCart }) => {
  const [showForm, setShowForm] = useState(true);
  const [cartItems, setCartItems] = useState([]);
  const initialValues = {
    name: "",
    lastname: "",
    age: "",
    address: "",
    phone: "",
  };

  const handleSubmit = (values, { resetForm }) => {
    console.log("Data from form:", values);

    const cartItem = {
      name: values.name,
      lastname: values.lastname,
      age: values.age,
      address: values.address,
      phone: values.phone,
    };
    addToCart(cartItem);
    resetForm();
    setShowForm(false);
  };

  const addToCart = (item) => {
    setCartItems([...cartItems, item]);
  };

  const clearCart = () => {
    setCartItems([]);
    setShowForm(true);
    onClearCart();
  };
  return (
    <div>
      {showForm ? (
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}
        >
          <Form className={styles.form}>
            <div className={styles.inputField}>
              <label htmlFor="name">Name</label>
              <Field
                type="text"
                id="name"
                name="name"
                className={styles.input}
                placeholder="Enter your name"
              />
              <ErrorMessage
                name="name"
                component="div"
                className={styles.error}
              />
            </div>
            <div className={styles.inputField}>
              <label htmlFor="lastname">Lastname</label>
              <Field
                type="text"
                id="lastname"
                name="lastname"
                className={styles.input}
                placeholder="Enter your lastname"
              />
              <ErrorMessage
                name="lastname"
                component="div"
                className={styles.error}
              />
            </div>
            <div className={styles.inputField}>
              <label htmlFor="age">Age</label>
              <Field
                type="number"
                id="age"
                name="age"
                className={styles.input}
                placeholder="Enter your age"
              />
              <ErrorMessage
                name="age"
                component="div"
                className={styles.error}
              />
            </div>
            <div className={styles.inputField}>
              <label htmlFor="address">Address</label>
              <Field
                type="text"
                id="address"
                name="address"
                className={styles.input}
                placeholder="Enter your address"
              />
              <ErrorMessage
                name="address"
                component="div"
                className={styles.error}
              />
            </div>
            <div className={styles.inputField}>
              <label htmlFor="phone">Phone</label>
              <Field name="phone">
                {({ field }) => (
                  <PatternFormat
                    {...field}
                    format="##########"
                    mask="_"
                    placeholder="Enter your phone"
                    className={styles.input}
                  />
                )}
              </Field>
              <ErrorMessage
                name="phone"
                component="div"
                className={styles.error}
              />
            </div>
            <button type="submit" className={styles.input}>
              Checkout
            </button>
          </Form>
        </Formik>
      ) : (
        <div>
          <h2>Your Shopping Cart</h2>
          {/* Відображення товарів у кошику */}
          <ul>
            {cartItems.map((item, index) => (
              <li key={index}>
                Name: {item.name}, Lastname: {item.lastname}, Age: {item.age},
                Address: {item.address}, Phone: {item.phone}
              </li>
            ))}
          </ul>
          <button onClick={clearCart}>Clear Cart</button>
        </div>
      )}
    </div>
  );
};

export default CheckoutForm;
