import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import Button from "./Button";
import Modal from "./Modal";
import style from "./styles/Modal.module.scss";
import styles from "./styles/Products.module.scss";

function Product({
  info,
  updateFavoriteCount,
  removeFromSelectedProducts,
  addToSelectedProducts,
  updateCartCount,
  addToCartProducts,
}) {
  const [showModal, setShowModal] = useState(false);
  const [isFavorite, setIsFavorite] = useState(false);
  const [isInCart, setIsInCart] = useState(false);

  useEffect(() => {
    const storedFavoriteStatus = localStorage.getItem(
      `favoriteStatus_${info.id}`
    );

    const storedInCartStatus = localStorage.getItem(`inCartStatus_${info.id}`);

    if (storedFavoriteStatus !== null) {
      setIsFavorite(JSON.parse(storedFavoriteStatus));
    }

    if (storedInCartStatus !== null) {
      setIsInCart(JSON.parse(storedInCartStatus));
    }
  }, [info.id]);

  useEffect(() => {
    localStorage.setItem(`inCartStatus_${info.id}`, JSON.stringify(isInCart));
    localStorage.setItem(
      `favoriteStatus_${info.id}`,
      JSON.stringify(isFavorite)
    );
  }, [info.id, isInCart, isFavorite]);

  const handleFavoriteClick = () => {
    setIsFavorite((prevIsFavorite) => !prevIsFavorite);
    updateFavoriteCount(isFavorite ? -1 : 1);
    isFavorite ? removeFromSelectedProducts(info) : addToSelectedProducts(info);
  };

  const handleAddToCartClick = () => {
    const newIsInCart = !isInCart;
    setIsInCart(true);
    if (newIsInCart) {
      updateCartCount(1);
      addToCartProducts(info);
    }
  };

  const openModal = () => {
    setShowModal(true);
  };

  const cancelButton = (
    <button
      className={style.modalButton}
      onClick={() => {
        setShowModal(false);
      }}
    >
      Cancel
    </button>
  );

  const saveButton = (
    <button
      className={style.modalButton}
      onClick={() => {
        handleAddToCartClick();
        setShowModal(false);
      }}
    >
      Ok
    </button>
  );

  const actionButtons = [saveButton, cancelButton];

  return (
    <>
      <div className={styles.card}>
        <button
          className={`${styles.favoriteButton} ${
            isFavorite ? styles.favorite : styles.star
          }`}
          onClick={() => handleFavoriteClick()}
        >
          <span>&#9733;</span>
        </button>
        <h2>{info.brandName}</h2>
        <p>{info.price} $</p>
        <img className={styles.imgCar} src={info.image} alt="" />
        <Button
          backgroundColor="lightgreen"
          text="Add to cart"
          heandler={openModal}
        />
      </div>

      <Modal
        header="Add to cart this product?"
        text="Are you sure you want to  cart this product?"
        actions={actionButtons}
        showModal={showModal}
        setShowModal={setShowModal}
        closeButton={() => setShowModal(false)}
      />
    </>
  );
}

Product.propTypes = {
  info: PropTypes.shape({
    id: PropTypes.number,
    brandName: PropTypes.string,
    price: PropTypes.string,
    image: PropTypes.string,
  }),
  updateFavoriteCount: PropTypes.func.isRequired,
  updateCartCount: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  deleteHandler: PropTypes.func.isRequired,
};

Product.defaultProps = {
  updateFavoriteCount: () => {},
  updateCartCount: () => {},
  handleClick: () => {},
  deleteHandler: () => {},
};

export default Product;
