import PropTypes from "prop-types";
import styles from "./styles/Buttons.module.scss";

function Button({ heandler, backgroundColor, text }) {
  return (
    <button
      onClick={heandler}
      style={{ backgroundColor: backgroundColor }}
      className={styles.button}
    >
      {text}
    </button>
  );
}

Button.propTypes = {
  heandler: PropTypes.func.isRequired,
  backgroundColor: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default Button;
