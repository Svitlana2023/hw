import PropTypes from "prop-types";

function FullScreenWrapper({ children }) {
  return <div>{children}</div>;
}

FullScreenWrapper.propTypes = {
  children: PropTypes.object.isRequired,
};

export default FullScreenWrapper;
