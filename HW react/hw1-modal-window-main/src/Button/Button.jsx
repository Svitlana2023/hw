// import styles from "./styles/Button.module.scss"
import styles from "./Button.module.scss";

function Button(props) {
  return (
    <button
      className={styles.button}
      style={{ backgroundColor: props.backgroundColor }}
      onClick={props.onClick}
    >
      {props.text}
    </button>
  );
}

export default Button;
