import FullScreenWrapper from "./components/FullScreenWrapper";
import "./main.scss";
import Product from "./components/Product";
import { useEffect, useState } from "react";
import Header from "./components/Header";
import Favorites from "./components/Favorites";
import { Route, Routes } from "react-router-dom";
import ProductsInCart from "./components/ProductsInCart";
import { useSelector, useDispatch } from "react-redux";
import { fetchProducts } from "./state/getProducts";

const style = {
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
};

// const handleClick = (event) => {
//   console.log(event.target);
// };

function App() {
  const [favoriteCount, setFavoriteCount] = useState(0);
  const [cartCount, setCartCount] = useState(0);
  const [products, setProducts] = useState([]);
  const [selectedCardId, setSelectedCardId] = useState(null);
  // const [selectedCardId, setSelectedCardId] = useState(null);
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [selectedProductsInCart, setSelectedProductsInCart] = useState([]);
  const state = useSelector((state) => state);
  // const productList = useSelector((state) => state?.productList);
  const dispatch = useDispatch();

  const addToCartProducts = (product) => {
    setSelectedProductsInCart([...selectedProductsInCart, product]);
    // const productAlreadyInCart = selectedProductsInCart.some(
    //   (cartProduct) => cartProduct.id === product.id
    // );
    // if (!productAlreadyInCart) {

    // }
  };

  const addToSelectedProducts = (product) => {
    setSelectedProducts([...selectedProducts, product]);
  };

  const removeFromCartProducts = (productId) => {
    const updatedProducts = selectedProductsInCart.filter(
      (p) => p.id !== productId
    );
    const deleteCard = window.confirm("Delete this card?");
    if (deleteCard === true) {
      setSelectedProductsInCart(updatedProducts);
      updateCartCount(-1);
    }
  };

  const removeFromSelectedProducts = (product) => {
    const updatedProducts = selectedProducts.filter((p) => p.id !== product.id);
    setSelectedProducts(updatedProducts);
  };

  useEffect(() => {
    const storedFavoriteCount = localStorage.getItem("favoriteCount");
    const storedInCartCount = localStorage.getItem("cartCount");

    if (storedFavoriteCount !== null) {
      setFavoriteCount(parseInt(storedFavoriteCount));
    }

    if (storedInCartCount !== null) {
      setCartCount(parseInt(storedInCartCount));
    }

    const storedSelectedProducts = localStorage.getItem("selectedProducts");
    if (storedSelectedProducts) {
      setSelectedProducts(JSON.parse(storedSelectedProducts));
    }
    const storedSelectedProductsInCart = localStorage.getItem(
      "selectedProductsInCart"
    );
    if (storedSelectedProductsInCart) {
      setSelectedProductsInCart(JSON.parse(storedSelectedProductsInCart));
    }
  }, []);
  useEffect(() => {
    // Замість використання fetch безпосередньо у компоненті, викликайте дію fetchProducts
    dispatch(fetchProducts());
  }, [dispatch]);
  // useEffect(() => {
  //   axios
  //     .get("/Products.json")
  //     .then((res) => {
  //       setProducts(res.data);
  //       localStorage.setItem("products", JSON.stringify(res.data));
  //     })
  //     .catch((err) => console.log(err));
  // }, []);

  useEffect(() => {
    const storedSelectedProductsInCart = localStorage.getItem(
      "selectedProductsInCart"
    );
    if (storedSelectedProductsInCart) {
      setSelectedProductsInCart(JSON.parse(storedSelectedProductsInCart));
    }

    const storedSelectedFavorites = localStorage.getItem(
      "selectedProductsInCart"
    );
    if (storedSelectedFavorites) {
      setSelectedProductsInCart(JSON.parse(storedSelectedFavorites));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem(
      "selectedProductsInCart",
      JSON.stringify(selectedProductsInCart)
    );
  }, [selectedProductsInCart]);

  useEffect(() => {
    localStorage.setItem("selectedProducts", JSON.stringify(selectedProducts));
  }, [selectedProducts]);

  const updateFavoriteCount = (countChange) => {
    const newFavoriteCount = favoriteCount + countChange;

    if (newFavoriteCount >= 0) {
      setFavoriteCount(newFavoriteCount);
      localStorage.setItem("favoriteCount", newFavoriteCount.toString());
    } else if (newFavoriteCount <= 0) {
      setFavoriteCount(0);
    }
  };

  const updateCartCount = (countChange) => {
    const newCartCount = cartCount + countChange;

    if (newCartCount >= 0) {
      setCartCount(newCartCount);
      localStorage.setItem("cartCount", newCartCount.toString());
    } else if (newCartCount <= 0) {
      setCartCount(0);
    }
  };
  const removeProduct = (id) => {
    const deleteCard = window.confirm("Delete this card?");
    if (deleteCard === true) {
      state(state.filter((p) => p.id !== id));
      // state(productList.filter((p) => p.id !== id));
    } else {
      state(state);
      // state(productList);
    }
  };

  const checkProductId = (id) => {
    setSelectedCardId(id);
  };

  const removeFromCart = (productId) => {
    const updatedSelectedProductsInCart = selectedProductsInCart.filter(
      (product) => product.id !== productId
    );
    setSelectedProductsInCart(updatedSelectedProductsInCart);
    setCartCount(cartCount - 1);
  };
  // const checkProductId = (id) => {
  //   setSelectedCardId(id);
  //   console.log(id);
  // };

  // const productsElements = products.map((p) => (
  //   <Product
  //     key={p.id}
  //     info={p}
  //     updateFavoriteCount={updateFavoriteCount}
  //     updateCartCount={updateCartCount}
  //     // handleClick={handleClick}
  //     // checkProductId={checkProductId}
  //     addToSelectedProducts={addToSelectedProducts}
  //     addToCartProducts={addToCartProducts}
  //     removeFromSelectedProducts={removeFromSelectedProducts}
  //   />
  // ));
  const productsElements = state ? (
    state.productList.map((p) => (
      <Product
        key={p.id}
        info={p}
        updateFavoriteCount={updateFavoriteCount}
        updateCartCount={updateCartCount}
        // handleClick={handleClick}
        deleteHandler={removeProduct}
        checkProductId={checkProductId}
        addToSelectedProducts={addToSelectedProducts}
        addToCartProducts={addToCartProducts}
        removeFromSelectedProducts={removeFromSelectedProducts}
      />
    ))
  ) : (
    <p>Loading...</p>
  );
  return (
    <>
      <Header favoriteCount={favoriteCount} cartCount={cartCount} />
      <Routes>
        <Route
          path="/"
          element={
            <FullScreenWrapper>
              <div style={style}>{productsElements}</div>
            </FullScreenWrapper>
          }
        />
        <Route
          path="/cart"
          element={
            <ProductsInCart
              selectedProducts={selectedProductsInCart}
              removeFromCartProducts={removeFromCartProducts}
            />
          }
        />
        <Route
          path="/favorites"
          element={<Favorites selectedProducts={selectedProducts} />}
        />
        <Route path="*" element={<div>Not found Route</div>} />
      </Routes>
    </>
  );
}

export default App;
