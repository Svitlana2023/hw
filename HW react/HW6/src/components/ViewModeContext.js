import React, { createContext, useContext, useState } from 'react';

const ViewModeContext = createContext();

export function useViewMode() {
  return useContext(ViewModeContext);
}

export function ViewModeProvider({ children }) {
  const [viewMode, setViewMode] = useState('cards'); // За замовчуванням показуємо в картах

  const toggleViewMode = () => {
    setViewMode(prevMode => (prevMode === 'cards' ? 'table' : 'cards'));
  };

  return (
    <ViewModeContext.Provider value={{ viewMode, toggleViewMode }}>
      {children}
    </ViewModeContext.Provider>
  );
}
