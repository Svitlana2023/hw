
import renderer from 'react-test-renderer';
import FullScreenWrapper from '../components/FullScreenWrapper';
import Button from '../components/Button';



test('test app component', () => {
    const component = renderer.create(<FullScreenWrapper />)
    const structure = component.toJSON()
    expect(structure).toMatchSnapshot()
})


test('button component snapshot', () =>{
    const comp = renderer.create(<Button label="{props.text}" onClick={() => {}} />)
    const tree = comp.toJSON();
    expect(tree).toMatchSnapshot()
})
