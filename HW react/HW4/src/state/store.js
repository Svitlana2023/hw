
import {configureStore} from "@reduxjs/toolkit";
import defaultReducer from './reducer';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from "redux";

// const store = configureStore({
//     reducer: defaultReducer
// })
const store = createStore(defaultReducer, applyMiddleware(thunk));

export default store