const tabsContainer = document.querySelector(".tabs");

tabsContainer.addEventListener("click", function (event) {
  const clickedElement = event.target;

  if (clickedElement.classList.contains("tabs-title")) {
    const tabs = tabsContainer.querySelectorAll(".tabs-title");
    const tabContents = document.querySelectorAll(".tabs-content li");

    const index = Array.from(tabs).indexOf(clickedElement);

    tabs.forEach(function (tab) {
      tab.classList.remove("active");
    });

    tabContents.forEach(function (tabContent) {
      tabContent.classList.remove("active");
    });

    clickedElement.classList.add("active");
    tabContents[index].classList.add("active");
  }
});
