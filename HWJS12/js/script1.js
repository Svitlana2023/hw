// Чому для роботи з input не рекомендується використовувати клавіатуру ?

//   Клавіатура може викликати неочікувану поведінку, клавіатури мають  різні мови, деякі клавіші мають певні функції та ін. А отже поведінка може бути інша ніж передбачає код.

const btnWrapper = document.querySelector(".btn-wrapper");
let activeButton = null;

function changeColor(button) {
  if (activeButton) {
    activeButton.style.backgroundColor = "black";
  }
  button.style.backgroundColor = "blue";
  activeButton = button;
}

btnWrapper.addEventListener("click", function (event) {
  changeColor(event.target);
});

document.addEventListener("keydown", function (event) {
  const key = event.key.toLowerCase();
  const targetButton = Array.from(btnWrapper.querySelectorAll(".btn")).find(
    function (button) {
      return button.textContent.toLowerCase() === key;
    }
  );
  if (targetButton) {
    changeColor(targetButton);
  }
});
