document.addEventListener("DOMContentLoaded", function () {
  const timerElement = document.createElement("div");
  timerElement.setAttribute("class", "timer");
  document.body.appendChild(timerElement);

  const imageToShowElements = document.querySelectorAll(".image-to-show");
  let currentImageIndex = 0;
  let timerInterval;
  let remainingTime = 3000;

  function updateTimer() {
    const seconds = Math.ceil(remainingTime / 1000); // Змінено на округлення вгору
    const milliseconds = remainingTime % 1000;

    timerElement.textContent =
      "Час до наступного зображення: " + seconds + "с " + milliseconds + "мс";
  }

  function showNextImage() {
    const currentImage = imageToShowElements[currentImageIndex];
    fadeOut(currentImage, 500, function () {
      currentImage.classList.remove("visible");
      currentImage.style.opacity = 1;

      currentImageIndex = (currentImageIndex + 1) % imageToShowElements.length;
      const nextImage = imageToShowElements[currentImageIndex];
      fadeIn(nextImage, 500, function () {
        nextImage.classList.add("visible");
      });
    });

    remainingTime = 3000;
    updateTimer();
  }

  function fadeOut(element, duration, callback) {
    let opacity = 1;
    const intervalTime = 10;
    const steps = duration / intervalTime;
    const delta = opacity / steps;

    const fadeOutInterval = setInterval(function () {
      opacity -= delta;
      element.style.opacity = opacity;

      if (opacity <= 0) {
        clearInterval(fadeOutInterval);
        callback();
      }
    }, intervalTime);
  }

  function fadeIn(element, duration, callback) {
    let opacity = 0;
    element.style.opacity = opacity;

    const intervalTime = 10;
    const steps = duration / intervalTime;
    const delta = 1 / steps;

    const fadeInInterval = setInterval(function () {
      opacity += delta;
      element.style.opacity = opacity;

      if (opacity >= 1) {
        clearInterval(fadeInInterval);
        callback();
      }
    }, intervalTime);
  }

  function startSlideshow() {
    timerInterval = setInterval(function () {
      remainingTime -= 10;
      if (remainingTime <= 0) {
        showNextImage();
      }
      updateTimer();
    }, 10);

    const stopButton = document.getElementById("stopButton");
    stopButton.style.display = "block";
    const resumeButton = document.getElementById("resumeButton");
    resumeButton.style.display = "none";
  }

  function stopSlideshow() {
    clearInterval(timerInterval);
    const stopButton = document.getElementById("stopButton");
    stopButton.style.display = "none";
    const resumeButton = document.getElementById("resumeButton");
    resumeButton.style.display = "block";
  }

  const stopButton = document.getElementById("stopButton");
  stopButton.addEventListener("click", stopSlideshow);
  const resumeButton = document.getElementById("resumeButton");
  resumeButton.addEventListener("click", startSlideshow);

  updateTimer();
  startSlideshow();
});
