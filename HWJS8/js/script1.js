// 1 Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const elements = document.querySelectorAll('p');
console.log('Перелік параграфів:', elements);

// for (let i = 0; i < elements.length; i = i + 1) {
//   elements[i].style.background = 'red';
// }

// for (let element of elements) {
//   element.style.background = 'red';
// }

elements.forEach(element => (element.style.background = 'red'));

// 2 Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const elem = document.getElementById('optionsList');
console.log('Елемент із id=optionsList:', elem);
const parrentEl = elem.parentNode;
console.log('Батьківський елемент:', parrentEl);
const childElements = elem.querySelectorAll('*');
let resultArr = [];
childElements.forEach(element => {
  let nodeType;
  switch (element.nodeType) {
    case 1:
      nodeType = 'ELEMENT_NODE';
      break;
    case 2:
      nodeType = 'ATTRIBUTE_NODE';
      break;
    case 3:
      nodeType = 'TEXT_NODE';
      break;
    case 4:
      nodeType = 'CDATA_SECTION_NODE';
      break;
    case 5:
      nodeType = 'ENTITY_REFERENCE_NODE';
      break;
    case 6:
      nodeType = 'ENTITY_NODE';
      break;
    case 7:
      nodeType = 'PROCESSING_INSTRUCTION_NODE';
      break;
    case 8:
      nodeType = 'COMMENT_NODE';
      break;
    case 9:
      nodeType = 'DOCUMENT_NODE';
      break;
    case 10:
      nodeType = 'DOCUMENT_TYPE_NODE';
      break;
    case 11:
      nodeType = 'DOCUMENT_FRAGMENT_NODE';
      break;
    case 12:
      nodeType = 'NOTATION_NODE';
      break;
  }
  resultArr.push({
    'Назва елемента': element.nodeName,
    'Тип елемента': nodeType,
  });
});
console.log('Дочірні ноди:');
console.table(resultArr);

// 3 Встановіть в якості контента елемента з id="testParagraph" наступний параграф - This is a paragraph
const newElement = document.getElementById('testParagraph');
newElement.textContent = 'This is a paragraph';

// 4. Отримати елементи, вкладені в елемент із класом main - header і вивести їх у консоль.Кожному з елементів присвоїти новий клас nav - item.
// const newСlass = document.querySelector('.main-header');
// const nestedElements = newСlass.querySelectorAll('*');
// console.log('Елементи із класом main - header:', nestedElements);
// nestedElements.forEach(element => {
//   element.classList.add('nav-item');
// });
// console.log('Елементи з новим класом nav - item:', nestedElements);

const newClass = document.querySelector('.main-header');
const nestedElements = newClass.children;
console.log('Елементи з класом main-header:', nestedElements);
for (let i = 0; i < nestedElements.length; i++) {
  nestedElements[i].classList.add('nav-item');
}
console.log('Елементи з новим класом nav-item:', nestedElements);

// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const deletedСlass = document.querySelectorAll('.section-title');
console.log('Елементи із класом section-title:', deletedСlass);
deletedСlass.forEach(title => {
  title.classList.remove('section-title');
});
console.log('Видалено клас section-title:', deletedСlass);
