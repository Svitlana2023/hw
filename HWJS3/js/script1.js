alert('Завдання 1.');
let inputNum;

do {
  inputNum = prompt('Введіть ціле число:');
} while (!inputNum || !Number.isInteger(Number(inputNum)));

let foundNums = false;
const multiple = 5;

for (let i = multiple; i <= inputNum; i++) {
  if (i % multiple === 0) {
    console.log(i);
    foundNums = true;
  }
}
if (!foundNums) {
  console.log('Sorry, no numbers');
}
