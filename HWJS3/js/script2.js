alert('Завдання 2.');
let m;
let n;
while (true) {
  m = parseInt(prompt('Введіть перше число:'));
  n = parseInt(prompt('Введіть друге число:'));

  if (m > n) {
    [m, n] = [n, m];
  }
  if (isNaN(m) || isNaN(n)) {
    alert('Ви ввели не вірне значення. Спробуйте ще раз.');
    continue;
  }
  if (m === n) {
    alert('Введіть різні числа. Спробуйте ще раз.');
    continue;
  }
  break;
}

for (let i = m; i <= n; i++) {
  let isPrime = true;
  let limit = Math.sqrt(Math.abs(i));
  for (let j = 2; j <= limit; j++) {
    if (i % j === 0) {
      isPrime = false;
      break;
    }
  }
  if (isPrime && Math.abs(i) >= 2) {
    console.log(i);
  }
}
