#1.Як можна оголосити змінну у JavaScript?
Зміну можна оголосити трьома способами з використанням:
* var - це старий спосіб оголошення змінної (зараз не застосовують, але вона є у старих кодах), цю змінну зараз повністю замінюють змінні, що нижче.
* let -оголошує змінну, значення якої потім можна буде змінити, перевизначити.
* const - оголошує змінну, значення якої потім не можна буде змінити, перевизначити.
Щоб прописати змінну ми спочатку вказуємо ключове слово let або const потім через пробіл  прописуємо назву змінної, потім знак дорівнює і вказуємо значення:
let x = 10;
const y = 2023;


#2.У чому різниця між функцією prompt та функцією confirm?


* функція confirm - виводить спливаюче вікно з повідомленням, у відповідь на яке можна натиснути лише Ok або Cancel. Тобто розгорнутої відповіді або текстового повідомлення від користувача отримати не можна.

* функція prompt-з допомогою неї можна отримати певну інформацію, тобто при її використанні з’являється поле для воду даних і відповідно можна отримати текстове повідомлення від користувача.

#3.Що таке неявне перетворення типів? Наведіть один приклад.

Неявне перетворення типів - це коли JavaScript значення  конвертує  між різними  типами даних в один тип автоматично без використання функцій.
Приклад: console.log("1"+2+3+4+5)
Оскільки перше значення є рядком, всі решта значення автоматично конвертуються у рядок, тобто результат дорівнює 12345.
