// ВАРІАНТ 1

// function filterBy(arr, type) {
//   const filteredArray = [];

//     for (let i = 0; i < arr.length; i = i + 1) {
//       if (typeof arr[i] !== type) {
//         filteredArray.push(arr[i]);
//       }
//     }

//   return filteredArray;
// }

// const inputArray = ['hello', 'world', 23, '23', null];
// const filteredArray = filterBy(inputArray, 'string');
// console.log(filteredArray);

// ВАРІАНТ 2

function filterBy(arr, type) {
  const filteredArray = [];

  arr.forEach(el => {
    if (typeof el !== type) {
      filteredArray.push(el);
    }
  });

  return filteredArray;
}

const inputArray = ['hello', 'world', 23, '23', null];
const filteredArray = filterBy(inputArray, 'string');
console.log(filteredArray);
