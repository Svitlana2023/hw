// Варіант 1
// let username = prompt("Введіть своє ім’я");
// while (!username) {
//   username = prompt("Введіть своє ім’я", username);
// }
// let age = prompt("Введіть ваш вік");
// while (!Number(age)) {
//   age = prompt("Введіть ваш вік", age);
// }
// if (age > 22) {
//   alert("Welcome, " + username + "!");
// } else if (age >= 18) {
//   const result = confirm("Are you sure you want to continue?");
//   if (result) {
//     alert("Welcome, " + username + "!");
//   } else {
//     alert("You are not allowed to visit this website.");
//   }
// } else {
//   alert("You are not allowed to visit this website.");
// }

// Варіант 2
let username;

do {
  username = prompt("Введіть своє ім’я", username)
} while (!username)
  
let age;

do {
  age = prompt("Введіть ваш вік", age)
} while (!Number(age)) 

age > 22 || (age >= 18 && confirm("Are you sure you want to continue?")) ?
  alert("Welcome, " + username + "!") : alert("You are not allowed to visit this website.");
